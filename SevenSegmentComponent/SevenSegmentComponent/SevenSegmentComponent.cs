﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Threading;



namespace SevenSegmentComponent
{
    [Serializable]


    /// <summary>
    ///The SevenSegmentComponent
    ///Contains all method affecting component operation
    /// </summary>
    public partial class SevenSegmentComponent : UserControl
    {

        private Color activeColor = Color.FromArgb(255, 0, 0);
        private Color inactiveColor = Color.FromArgb(90, 211, 211, 211);

        private Color lightBackgroundColor = Color.FromArgb(220, 220, 220);
        private Color darkBackgroundColor = Color.FromArgb(0, 0, 0);
        private bool gleamState = true;
        /// <summary>
        /// Default size set
        /// </summary>
        public Size size = new Size(120, 200);

        int[] segment = new int[8];

        /// <summary>
        /// This method:
        /// Set Size = new Size(120, 200);
        /// BorderStyle = BorderStyle.None;
        /// And initializeComponent
        /// </summary>
        public SevenSegmentComponent()
        {
            Size = new Size(120, 200);
            BorderStyle = BorderStyle.None;
            InitializeComponent();
        }
        /// <summary>
        /// This method allows you to add a component to the container
        /// </summary>
        /// <param name="container">indicates a specific container</param>
        public SevenSegmentComponent(IContainer container)
        {
            container.Add(this);
            InitializeComponent();
        }

        /// <summary>
        /// This method retrun activeColor component 
        /// Allows you to change the color of the display
        /// Invalidate() draws the display again
        /// </summary>
        public Color ChangeDisplayColor
        {
            get { return activeColor; }
            set { activeColor = value; Invalidate(); }
        }

        /// <summary>
        /// This method retrun present BorderStyle component 
        /// Allows you to change the BorderStyle of the display
        /// Invalidate() draws the display again
        /// </summary>
        public BorderStyle ChangeFrameStyle
        {
            get { return BorderStyle; }
            set { BorderStyle = value; Invalidate(); }
        }
        /// <summary>
        /// This method allows you to change Background Color
        /// Invalidate() draws the display again
        /// </summary>
        public Color ChangeBackgroundColor
        {
            get { return default;  }
            set { lightBackgroundColor = value; Invalidate(); }
        }
        /// <summary>
        /// This method allows power gleam, after power create new Thread whitch runs the gleam method
        /// Invalidate() draws the display again
        /// </summary>
        public bool ChangeGleam
        {
            get { return gleamState; }
            set
            {
                gleamState = value;
                Thread gleamThread;
                if (gleamState == false)
                {
                    gleamThread = new Thread(Gleam);
                    gleamThread.Start();
                }
                Invalidate();
            }
        }
        /// <summary>
        /// This method save active color in the variable currentColor after which the thread alternately changes color active
        /// Invalidate() draws the display again
        /// </summary>
        public void Gleam()
        {
            Color currentColor = activeColor;
            while (!gleamState)
            {
                Thread.Sleep(250);
                if (activeColor != Color.Empty)
                {
                    activeColor = Color.Empty;
                }
                else
                {
                    activeColor = currentColor;
                }
                Invalidate();
            }
            activeColor = currentColor;
        }

        /// <summary>
        /// This method allows upon selecting which segments are to be lit.
        /// bool isNumeric contains Regex checking the correctness of the entered value
        /// /// Invalidate() draws the display again
        /// </summary>
        public string SetNumber 
        {
            set 
            {
                bool isNumeric = value != null && Regex.Match(value, "^\\d.?$").Success ? true : false;
             
                if (isNumeric)
                {
                    char digit = value[0];
                    char dot = '0';
                    if (value.Length == 2)
                    {
                        dot = value[1];
                    }

                    switch (digit)
                    {

                        case '1':
                            segment[0] = 0;
                            segment[1] = 1;
                            segment[2] = 1;
                            segment[3] = 0;
                            segment[4] = 0;
                            segment[5] = 0;
                            segment[6] = 0;
                            segment[7] = 0;
                            break;
                        case '2':
                            segment[0] = 1;
                            segment[1] = 1;
                            segment[2] = 0;
                            segment[3] = 1;
                            segment[4] = 1;
                            segment[5] = 0;
                            segment[6] = 1;
                            segment[7] = 0;
                            break;
                        case '3':
                            segment[0] = 1;
                            segment[1] = 1;
                            segment[2] = 1;
                            segment[3] = 1;
                            segment[4] = 0;
                            segment[5] = 0;
                            segment[6] = 1;
                            segment[7] = 0;
                            break;
                        case '4':
                            segment[0] = 0;
                            segment[1] = 1;
                            segment[2] = 1;
                            segment[3] = 0;
                            segment[4] = 0;
                            segment[5] = 1;
                            segment[6] = 1;
                            segment[7] = 0;
                            break;
                        case '5':
                            segment[0] = 1;
                            segment[1] = 0;
                            segment[2] = 1;
                            segment[3] = 1;
                            segment[4] = 0;
                            segment[5] = 1;
                            segment[6] = 1;
                            segment[7] = 0;
                            break;
                        case '6':
                            segment[0] = 1;
                            segment[1] = 0;
                            segment[2] = 1;
                            segment[3] = 1;
                            segment[4] = 1;
                            segment[5] = 1;
                            segment[6] = 1;
                            segment[7] = 0;
                            break;
                        case '7':
                            segment[0] = 1;
                            segment[1] = 1;
                            segment[2] = 1;
                            segment[3] = 0;
                            segment[4] = 0;
                            segment[5] = 0;
                            segment[6] = 1;
                            segment[7] = 0;
                            break;
                        case '8':
                            segment[0] = 1;
                            segment[1] = 1;
                            segment[2] = 1;
                            segment[3] = 1;
                            segment[4] = 1;
                            segment[5] = 1;
                            segment[6] = 1;
                            segment[7] = 0;
                            break;
                        case '9':
                            segment[0] = 1;
                            segment[1] = 1;
                            segment[2] = 1;
                            segment[3] = 1;
                            segment[4] = 0;
                            segment[5] = 1;
                            segment[6] = 1;
                            segment[7] = 0;
                            break;
                        case '0':
                            segment[0] = 1;
                            segment[1] = 1;
                            segment[2] = 1;
                            segment[3] = 1;
                            segment[4] = 1;
                            segment[5] = 1;
                            segment[6] = 0;
                            segment[7] = 0;
                            break;
                    }
                    if (dot == '.')
                    {
                        segment[7] = 1;
                    }
                } else
                {
                    switch (value)
                    {
                        case null:
                            segment[0] = 0;
                            segment[1] = 0;
                            segment[2] = 0;
                            segment[3] = 0;
                            segment[4] = 0;
                            segment[5] = 0;
                            segment[6] = 0;
                            segment[7] = 0;
                            break;
                        case "e":
                            segment[0] = 1;
                            segment[1] = 0;
                            segment[2] = 0;
                            segment[3] = 1;
                            segment[4] = 1;
                            segment[5] = 1;
                            segment[6] = 1;
                            segment[7] = 0;
                            break;
                        case "r":
                            segment[0] = 0;
                            segment[1] = 0;
                            segment[2] = 0;
                            segment[3] = 0;
                            segment[4] = 1;
                            segment[5] = 0;
                            segment[6] = 1;
                            segment[7] = 0;
                            break;
                        case "o":
                            segment[0] = 0;
                            segment[1] = 0;
                            segment[2] = 1;
                            segment[3] = 1;
                            segment[4] = 1;
                            segment[5] = 0;
                            segment[6] = 1;
                            segment[7] = 0;
                            break;
                        case "-":
                            segment[0] = 0;
                            segment[1] = 0;
                            segment[2] = 0;
                            segment[3] = 0;
                            segment[4] = 0;
                            segment[5] = 0;
                            segment[6] = 1;
                            segment[7] = 0;
                            break;
                    }
                }

                Invalidate();
            }
        }
        /// <summary>
        /// This method print 7 segment dipsplay with dot
        /// </summary>
        /// <param name="e">Provides data for the Control.Paint event</param>
        protected override void OnPaint(PaintEventArgs e)
        {

            Brush brush1 = new SolidBrush(activeColor);
            Brush brush2 = new SolidBrush(inactiveColor);
            Brush brush3 = new SolidBrush(lightBackgroundColor);

            int height = size.Height;
            int width = size.Width;
            int x = height / 200;
            int y = width / 120;

            Rectangle background = new Rectangle(0, 0, 120, 200);
            e.Graphics.FillRectangle(brush3, background);

            // top
            Point t1 = new Point(20 * x, 0 * y);
            Point t2 = new Point(80 * x, 0 * y);
            Point t3 = new Point(90 * x, 10 * y);
            Point t4 = new Point(80 * x, 20 * y);
            Point t5 = new Point(20 * x, 20 * y);
            Point t6 = new Point(10 * x, 10 * y);
            Point[] points1 = { t1, t2, t3, t4, t5, t6 };
            e.Graphics.FillPolygon(segment[0] == 1 ? brush1 : brush2, points1);

            // top left
            Point tl1 = new Point(10 * x, 15 * y);
            Point tl2 = new Point(20 * x, 25 * y);
            Point tl3 = new Point(20 * x, 85 * y);
            Point tl4 = new Point(10 * x, 95 * y);
            Point tl5 = new Point(0 * x, 85 * y);
            Point tl6 = new Point(0 * x, 25 * y);
            Point[] points2 = { tl1, tl2, tl3, tl4, tl5, tl6 };
            e.Graphics.FillPolygon(segment[5] == 1 ? brush1 : brush2, points2);


            // top right
            Point tr1 = new Point(90 * x, 15 * y);
            Point tr2 = new Point(100 * x, 25 * y);
            Point tr3 = new Point(100 * x, 85 * y);
            Point tr4 = new Point(90 * x, 95 * y);
            Point tr5 = new Point(80 * x, 85 * y);
            Point tr6 = new Point(80 * x, 25 * y);
            Point[] points3 = { tr1, tr2, tr3, tr4, tr5, tr6 };
            e.Graphics.FillPolygon(segment[1] == 1 ? brush1 : brush2, points3);

            // center
            Point c1 = new Point(20 * x, 90 * y);
            Point c2 = new Point(80 * x, 90 * y);
            Point c3 = new Point(90 * x, 100 * y);
            Point c4 = new Point(80 * x, 110 * y);
            Point c5 = new Point(20 * x, 110 * y);
            Point c6 = new Point(10 * x, 100 * y);
            Point[] points4 = { c1, c2, c3, c4, c5, c6 };
            e.Graphics.FillPolygon(segment[6] == 1 ? brush1 : brush2, points4);

            // bottom
            Point b1 = new Point(20 * x, 180 * y);
            Point b2 = new Point(80 * x, 180 * y);
            Point b3 = new Point(90 * x, 190 * y);
            Point b4 = new Point(80 * x, 200 * y);
            Point b5 = new Point(20 * x, 200 * y);
            Point b6 = new Point(10 * x, 190 * y);
            Point[] points5 = { b1, b2, b3, b4, b5, b6 };
            e.Graphics.FillPolygon(segment[3] == 1 ? brush1 : brush2, points5);

            // bottom left
            Point bl1 = new Point(10 * x, 105 * y);
            Point bl2 = new Point(20 * x, 115 * y);
            Point bl3 = new Point(20 * x, 175 * y);
            Point bl4 = new Point(10 * x, 185 * y);
            Point bl5 = new Point(0 * x, 175 * y);
            Point bl6 = new Point(0 * x, 115 * y);
            Point[] points6 = { bl1, bl2, bl3, bl4, bl5, bl6 };
            e.Graphics.FillPolygon(segment[4] == 1 ? brush1 : brush2, points6);

            // bottom right
            Point br1 = new Point(90 * x, 105 * y);
            Point br2 = new Point(100 * x, 115 * y);
            Point br3 = new Point(100 * x, 175 * y);
            Point br4 = new Point(90 * x, 185 * y);
            Point br5 = new Point(80 * x, 175 * y);
            Point br6 = new Point(80 * x, 115 * y);
            Point[] points7 = { br1, br2, br3, br4, br5, br6 };
            e.Graphics.FillPolygon(segment[2] == 1 ? brush1 : brush2, points7);

            // dot
            Point d1 = new Point(100 * x, 190 * y);
            Point d2 = new Point(103 * x, 183 * y);
            Point d3 = new Point(110 * x, 180 * y);
            Point d4 = new Point(117 * x, 183 * y);
            Point d5 = new Point(120 * x, 190 * y);
            Point d6 = new Point(117 * x, 197 * y);
            Point d7 = new Point(110 * x, 200 * y);
            Point d8 = new Point(103 * x, 197 * y); 
            Point[] points8 = { d1, d2, d3, d4, d5, d6, d7, d8 };
            e.Graphics.FillPolygon(segment[7] == 1 ? brush1 : brush2, points8);
        }

    }
}