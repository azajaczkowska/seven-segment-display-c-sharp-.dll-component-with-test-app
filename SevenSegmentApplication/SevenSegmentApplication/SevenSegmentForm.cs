﻿using System;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace SevenSegmentApplication
{
    public partial class sevenSegmentDisplayForm : Form
    {

        Color dark = Color.FromArgb(180, 180, 180);
        Color light = Color.FromArgb(220, 220, 220);


        public sevenSegmentDisplayForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string displayNumber = numberTextBox.Text;
            clearAllFields();
            if (!Regex.Match(displayNumber,"^-?([0-9]*[.])?[0-9]+$").Success)
            {
                showError();
                Invalidate();
                return;
            }
            int controlCounter = 0;
            if (displayNumber.Contains("."))
            {
                displayNumber = swapDot(displayNumber);
            }
            for (int i = 0; i < displayNumber.Length; i++)
            {

                SevenSegmentComponent.SevenSegmentComponent component = (SevenSegmentComponent.SevenSegmentComponent)hexDisplayPanel.Controls[controlCounter];
                string digit = displayNumber[displayNumber.Length - 1 - i].ToString();
                int nextIndex = displayNumber.Length - 2 - i;


                if (nextIndex >= 0 && displayNumber[nextIndex].Equals('.'))
                {
                
                    digit += displayNumber[nextIndex];
                    i++;
                }
                controlCounter++;
                component.SetNumber = digit;
            }

            Invalidate();
        }
        
        private string swapDot(string displayNumber)
        {
            int dotPosition = displayNumber.IndexOf(".");
            int afterPosition = dotPosition - 1;
            if(afterPosition >= 0) { 
            char charToReplace = displayNumber[afterPosition];
            displayNumber = displayNumber.Replace('.', charToReplace);
            displayNumber = displayNumber.Insert(afterPosition, ".");
            displayNumber = displayNumber.Remove(afterPosition+1,1);
            return displayNumber;
            } else
            {
            showError();
            return "";
            }
        }

        private void clearAllFields()
        {
            foreach(SevenSegmentComponent.SevenSegmentComponent component in hexDisplayPanel.Controls)
            {
                component.SetNumber = null;
            }
        }

        private void showError()
        {
            sevenSegmentComponent2.SetNumber = "e";
            sevenSegmentComponent1.SetNumber = "r";
            sevenSegmentComponent3.SetNumber = "r";
            sevenSegmentComponent4.SetNumber = "o";
            sevenSegmentComponent5.SetNumber = "r";
        }

        private void colorComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            string colorSelected = colorComboBox.Text;
            switch (colorSelected)
            {
                case "Czerwony":
                    setSegmentsColor(Color.Red);
                    break;
                case "Zielony":
                    setSegmentsColor(Color.Green);
                    break;
                case "Niebieski":
                    setSegmentsColor(Color.Blue);
                    break;
            }
        }

        private void frameComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (frameComboBox.Text.Equals("Brak"))
            {
                changeSegmentBorderStyle(BorderStyle.None);
            } else {
                changeSegmentBorderStyle(BorderStyle.Fixed3D);
            }
        }

        private void l2_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void backgroundComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            string backgroundSelected = backgroundComboBox.Text;

            if (backgroundComboBox.Text.Equals("Jasny"))
            {
                hexDisplayPanel.BackColor = light;
                changeDisplayBgColor(light);
            }
            else
            {
                hexDisplayPanel.BackColor = dark;
                changeDisplayBgColor(dark);
            }
        }

        private void gleamComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (gleamComboBox.Text.Equals("Wyłącz"))
            {
                setGleam(true);
            }
            else
            {
                setGleam(false);
            } 
        }

        private void setGleam(bool value)
        {
            foreach (SevenSegmentComponent.SevenSegmentComponent component in hexDisplayPanel.Controls)
            {
                component.ChangeGleam = value;
            }
        }

        private void sevenSegmentDisplayForm_Load(object sender, EventArgs e)
        {

        }

        private void setSegmentsColor(Color color)
        {
            foreach (SevenSegmentComponent.SevenSegmentComponent component in hexDisplayPanel.Controls)
            {
                component.ChangeDisplayColor = color;
            }
        }

        private void changeSegmentBorderStyle(BorderStyle style)
        {
            foreach (SevenSegmentComponent.SevenSegmentComponent component in hexDisplayPanel.Controls)
            {
                component.ChangeFrameStyle = style;
            }
        }

        private void changeDisplayBgColor(Color color)
        {
            foreach (SevenSegmentComponent.SevenSegmentComponent component in hexDisplayPanel.Controls)
            {
                component.BackColor = color;
            }
        }

        private void formResize(object sender, EventArgs e)
        {
            Control control = (Control)sender;

            int heightBefor = hexDisplayPanel.Size.Height;
            if (control.Size.Height != heightBefor)
                Console.WriteLine("zmiana rozmiaru");

            foreach (SevenSegmentComponent.SevenSegmentComponent component in hexDisplayPanel.Controls)
            {

            }   
        }
    }
}