﻿namespace SevenSegmentApplication
{
    partial class sevenSegmentDisplayForm
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.acceptButton = new System.Windows.Forms.Button();
            this.numberTextBox = new System.Windows.Forms.TextBox();
            this.l2 = new System.Windows.Forms.Label();
            this.l3 = new System.Windows.Forms.Label();
            this.hexDisplayPanel = new System.Windows.Forms.Panel();
            this.sevenSegmentComponent5 = new SevenSegmentComponent.SevenSegmentComponent(this.components);
            this.sevenSegmentComponent4 = new SevenSegmentComponent.SevenSegmentComponent(this.components);
            this.sevenSegmentComponent3 = new SevenSegmentComponent.SevenSegmentComponent(this.components);
            this.sevenSegmentComponent1 = new SevenSegmentComponent.SevenSegmentComponent(this.components);
            this.sevenSegmentComponent2 = new SevenSegmentComponent.SevenSegmentComponent(this.components);
            this.colorComboBox = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.gleamComboBox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.backgroundComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.frameComboBox = new System.Windows.Forms.ComboBox();
            this.l4 = new System.Windows.Forms.Label();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.groupBox1.SuspendLayout();
            this.hexDisplayPanel.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.acceptButton);
            this.groupBox1.Controls.Add(this.numberTextBox);
            this.groupBox1.Controls.Add(this.l2);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(307, 180);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Wprowadź dane";
            // 
            // acceptButton
            // 
            this.acceptButton.Location = new System.Drawing.Point(170, 74);
            this.acceptButton.Name = "acceptButton";
            this.acceptButton.Size = new System.Drawing.Size(120, 23);
            this.acceptButton.TabIndex = 7;
            this.acceptButton.Text = "Wyświetl";
            this.acceptButton.UseVisualStyleBackColor = true;
            this.acceptButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // numberTextBox
            // 
            this.numberTextBox.AcceptsTab = true;
            this.numberTextBox.Location = new System.Drawing.Point(169, 38);
            this.numberTextBox.MaxLength = 5;
            this.numberTextBox.Name = "numberTextBox";
            this.numberTextBox.Size = new System.Drawing.Size(121, 20);
            this.numberTextBox.TabIndex = 6;
            // 
            // l2
            // 
            this.l2.AutoSize = true;
            this.l2.Location = new System.Drawing.Point(14, 41);
            this.l2.Name = "l2";
            this.l2.Size = new System.Drawing.Size(138, 13);
            this.l2.TabIndex = 2;
            this.l2.Text = "Wpisz cyfry do wyświetlenia";
            this.l2.Click += new System.EventHandler(this.l2_Click);
            // 
            // l3
            // 
            this.l3.AutoSize = true;
            this.l3.Location = new System.Drawing.Point(20, 38);
            this.l3.Name = "l3";
            this.l3.Size = new System.Drawing.Size(99, 13);
            this.l3.TabIndex = 3;
            this.l3.Text = "Kolor wyświetlacza:";
            // 
            // hexDisplayPanel
            // 
            this.hexDisplayPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.hexDisplayPanel.AutoSize = true;
            this.hexDisplayPanel.BackColor = System.Drawing.Color.Gainsboro;
            this.hexDisplayPanel.Controls.Add(this.sevenSegmentComponent5);
            this.hexDisplayPanel.Controls.Add(this.sevenSegmentComponent4);
            this.hexDisplayPanel.Controls.Add(this.sevenSegmentComponent3);
            this.hexDisplayPanel.Controls.Add(this.sevenSegmentComponent1);
            this.hexDisplayPanel.Controls.Add(this.sevenSegmentComponent2);
            this.hexDisplayPanel.Location = new System.Drawing.Point(12, 198);
            this.hexDisplayPanel.Name = "hexDisplayPanel";
            this.hexDisplayPanel.Size = new System.Drawing.Size(650, 226);
            this.hexDisplayPanel.TabIndex = 16;
            // 
            // sevenSegmentComponent5
            // 
            this.sevenSegmentComponent5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.sevenSegmentComponent5.ChangeBackgroundColor = System.Drawing.Color.Empty;
            this.sevenSegmentComponent5.ChangeDisplayColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.sevenSegmentComponent5.ChangeFrameStyle = System.Windows.Forms.BorderStyle.None;
            this.sevenSegmentComponent5.ChangeGleam = true;
            this.sevenSegmentComponent5.Location = new System.Drawing.Point(527, 15);
            this.sevenSegmentComponent5.Name = "sevenSegmentComponent5";
            this.sevenSegmentComponent5.Size = new System.Drawing.Size(120, 201);
            this.sevenSegmentComponent5.TabIndex = 7;
            // 
            // sevenSegmentComponent4
            // 
            this.sevenSegmentComponent4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.sevenSegmentComponent4.ChangeBackgroundColor = System.Drawing.Color.Empty;
            this.sevenSegmentComponent4.ChangeDisplayColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.sevenSegmentComponent4.ChangeFrameStyle = System.Windows.Forms.BorderStyle.None;
            this.sevenSegmentComponent4.ChangeGleam = true;
            this.sevenSegmentComponent4.Location = new System.Drawing.Point(401, 15);
            this.sevenSegmentComponent4.Name = "sevenSegmentComponent4";
            this.sevenSegmentComponent4.Size = new System.Drawing.Size(120, 201);
            this.sevenSegmentComponent4.TabIndex = 6;
            // 
            // sevenSegmentComponent3
            // 
            this.sevenSegmentComponent3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.sevenSegmentComponent3.ChangeBackgroundColor = System.Drawing.Color.Empty;
            this.sevenSegmentComponent3.ChangeDisplayColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.sevenSegmentComponent3.ChangeFrameStyle = System.Windows.Forms.BorderStyle.None;
            this.sevenSegmentComponent3.ChangeGleam = true;
            this.sevenSegmentComponent3.Location = new System.Drawing.Point(275, 15);
            this.sevenSegmentComponent3.Name = "sevenSegmentComponent3";
            this.sevenSegmentComponent3.Size = new System.Drawing.Size(120, 201);
            this.sevenSegmentComponent3.TabIndex = 5;
            // 
            // sevenSegmentComponent1
            // 
            this.sevenSegmentComponent1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.sevenSegmentComponent1.ChangeBackgroundColor = System.Drawing.Color.Empty;
            this.sevenSegmentComponent1.ChangeDisplayColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.sevenSegmentComponent1.ChangeFrameStyle = System.Windows.Forms.BorderStyle.None;
            this.sevenSegmentComponent1.ChangeGleam = true;
            this.sevenSegmentComponent1.Location = new System.Drawing.Point(149, 15);
            this.sevenSegmentComponent1.Name = "sevenSegmentComponent1";
            this.sevenSegmentComponent1.Size = new System.Drawing.Size(120, 201);
            this.sevenSegmentComponent1.TabIndex = 4;
            // 
            // sevenSegmentComponent2
            // 
            this.sevenSegmentComponent2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.sevenSegmentComponent2.ChangeBackgroundColor = System.Drawing.Color.Empty;
            this.sevenSegmentComponent2.ChangeDisplayColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.sevenSegmentComponent2.ChangeFrameStyle = System.Windows.Forms.BorderStyle.None;
            this.sevenSegmentComponent2.ChangeGleam = true;
            this.sevenSegmentComponent2.Location = new System.Drawing.Point(23, 15);
            this.sevenSegmentComponent2.Name = "sevenSegmentComponent2";
            this.sevenSegmentComponent2.Size = new System.Drawing.Size(120, 201);
            this.sevenSegmentComponent2.TabIndex = 3;
            // 
            // colorComboBox
            // 
            this.colorComboBox.FormattingEnabled = true;
            this.colorComboBox.Items.AddRange(new object[] {
            "Czerwony",
            "Zielony",
            "Niebieski"});
            this.colorComboBox.Location = new System.Drawing.Point(175, 35);
            this.colorComboBox.Name = "colorComboBox";
            this.colorComboBox.Size = new System.Drawing.Size(137, 21);
            this.colorComboBox.TabIndex = 6;
            this.colorComboBox.SelectedIndexChanged += new System.EventHandler(this.colorComboBox_SelectedIndexChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.gleamComboBox);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.backgroundComboBox);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.l3);
            this.groupBox2.Controls.Add(this.colorComboBox);
            this.groupBox2.Controls.Add(this.frameComboBox);
            this.groupBox2.Controls.Add(this.l4);
            this.groupBox2.Location = new System.Drawing.Point(325, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(337, 180);
            this.groupBox2.TabIndex = 15;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Personalizuj wygląd";
            // 
            // gleamComboBox
            // 
            this.gleamComboBox.FormattingEnabled = true;
            this.gleamComboBox.Items.AddRange(new object[] {
            "Włącz",
            "Wyłącz"});
            this.gleamComboBox.Location = new System.Drawing.Point(175, 116);
            this.gleamComboBox.Name = "gleamComboBox";
            this.gleamComboBox.Size = new System.Drawing.Size(137, 21);
            this.gleamComboBox.TabIndex = 11;
            this.gleamComboBox.SelectedIndexChanged += new System.EventHandler(this.gleamComboBox_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 119);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Migotanie";
            // 
            // backgroundComboBox
            // 
            this.backgroundComboBox.FormattingEnabled = true;
            this.backgroundComboBox.Items.AddRange(new object[] {
            "Jasny",
            "Ciemny"});
            this.backgroundComboBox.Location = new System.Drawing.Point(175, 62);
            this.backgroundComboBox.Name = "backgroundComboBox";
            this.backgroundComboBox.Size = new System.Drawing.Size(137, 21);
            this.backgroundComboBox.TabIndex = 9;
            this.backgroundComboBox.SelectedIndexChanged += new System.EventHandler(this.backgroundComboBox_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 65);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Kolor tła";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // frameComboBox
            // 
            this.frameComboBox.FormattingEnabled = true;
            this.frameComboBox.Items.AddRange(new object[] {
            "Brak",
            "Wklęsła"});
            this.frameComboBox.Location = new System.Drawing.Point(175, 89);
            this.frameComboBox.Name = "frameComboBox";
            this.frameComboBox.Size = new System.Drawing.Size(137, 21);
            this.frameComboBox.TabIndex = 7;
            this.frameComboBox.SelectedIndexChanged += new System.EventHandler(this.frameComboBox_SelectedIndexChanged);
            // 
            // l4
            // 
            this.l4.AutoSize = true;
            this.l4.Location = new System.Drawing.Point(20, 92);
            this.l4.Name = "l4";
            this.l4.Size = new System.Drawing.Size(44, 13);
            this.l4.TabIndex = 4;
            this.l4.Text = "Ramka:";
            // 
            // sevenSegmentDisplayForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(675, 432);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.hexDisplayPanel);
            this.Controls.Add(this.groupBox2);
            this.Name = "sevenSegmentDisplayForm";
            this.Text = "Wyświetlacz siedmiosegmentowy";
            this.Load += new System.EventHandler(this.sevenSegmentDisplayForm_Load);
            this.Resize += new System.EventHandler(this.formResize);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.hexDisplayPanel.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox numberTextBox;
        private System.Windows.Forms.Label l2;
        private System.Windows.Forms.Label l3;
        private System.Windows.Forms.Panel hexDisplayPanel;
        private System.Windows.Forms.ComboBox colorComboBox;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox frameComboBox;
        private System.Windows.Forms.Label l4;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private SevenSegmentComponent.SevenSegmentComponent sevenSegmentComponent2;
        private SevenSegmentComponent.SevenSegmentComponent sevenSegmentComponent5;
        private SevenSegmentComponent.SevenSegmentComponent sevenSegmentComponent4;
        private SevenSegmentComponent.SevenSegmentComponent sevenSegmentComponent3;
        private SevenSegmentComponent.SevenSegmentComponent sevenSegmentComponent1;
        private System.Windows.Forms.Button acceptButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox gleamComboBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox backgroundComboBox;
    }
}

